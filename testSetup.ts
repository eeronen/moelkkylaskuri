import * as mockAsyncStorage from "@react-native-async-storage/async-storage/jest/async-storage-mock";
import {Fragment} from "react";

jest.mock("@react-native-async-storage/async-storage", () => mockAsyncStorage);
jest.mock("react-native-localize", () => ({
    findBestAvailableLanguage: () => undefined,
}));
jest.mock("react-native-linear-gradient", () => ({
    LinearGradient: () => Fragment,
}));
