import * as React from "react";
import {render as originalRender} from "@testing-library/react-native";
import {StoreState, makeStore} from "./react-app/store";
import {Provider} from "react-redux";
import {Store} from "@reduxjs/toolkit";
import {playersActions} from "./react-app/store/Players";

const ProviderWrapper: React.FC<
    React.PropsWithChildren<{store?: Store<StoreState>}>
> = props => (
    <Provider store={props.store ?? makeStore()}>{props.children}</Provider>
);

export const render = (
    component: React.ReactElement,
    customStore?: Store<StoreState>,
) =>
    originalRender(
        <ProviderWrapper store={customStore}>{component}</ProviderWrapper>,
    );

export const TEST_PLAYER_NAME = "player";

/**
 * Returns a store that has been prepped for testing
 */
export const getTestStore = (numberOfPlayers = 2) => {
    const store = makeStore();
    for (let index = 0; index < numberOfPlayers; index++) {
        store.dispatch(playersActions.addPlayer(TEST_PLAYER_NAME + index));
    }
    return store;
};

export const getTestId = (playerName: string, componentName: string) =>
    `${playerName}:${componentName}`;
