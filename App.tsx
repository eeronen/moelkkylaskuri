import React from "react";
import {SafeAreaView, StatusBar, Text} from "react-native";

import {Provider, useSelector} from "react-redux";
import {store, StoreState} from "./react-app/store";
import {ApplicationView} from "./react-app/store/Application";
import {AddPlayersView} from "./react-app/Views/AddPlayers";
import {useColors} from "./react-app/Views/Styles";
import {PlayView} from "./react-app/Views/Play";
import {ResultsView} from "./react-app/Views/Results";
import {StorageConnector} from "./react-app/Views/StorageConnector";
import {SettingsView} from "./react-app/Views/Settings";
import {useSettings} from "./react-app/Views/SettingsUtils";
import {StartMessage} from "./react-app/Views/StartMessage";
import {StringsProvider} from "./react-app/Localization/StringContext";

const App: React.FC<{}> = () => {
    const colors = useColors();
    const settings = useSettings();
    const backgroundStyle = {
        backgroundColor: colors.backgroundGreen,
    };

    new StorageConnector();

    return (
        <SafeAreaView style={backgroundStyle}>
            <Provider store={store}>
                <StringsProvider>
                    <StatusBar
                        barStyle={
                            settings.colorTheme === "light"
                                ? "light-content"
                                : "dark-content"
                        }
                        backgroundColor={colors.green}
                    />
                    <Application />
                </StringsProvider>
            </Provider>
        </SafeAreaView>
    );
};

export const Application: React.FC<{}> = () => {
    const currentView = useSelector(
        (state: StoreState) => state.application.currentView,
    );
    if (currentView === ApplicationView.PlayerSelection) {
        return <AddPlayersView />;
    } else if (currentView === ApplicationView.Playing) {
        return <PlayView />;
    } else if (currentView === ApplicationView.EndResults) {
        return <ResultsView />;
    } else if (currentView === ApplicationView.Settings) {
        return <SettingsView />;
    } else if (currentView === ApplicationView.StartMessage) {
        return <StartMessage />;
    } else {
        return <Text>Unknown error</Text>;
    }
};

export default App;
