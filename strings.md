My language skills are limited, so your help is needed to translate the app. The following list contains all the texts that the app currently has. If you are fluent in a language that is not yet on the list, you can send me an email to molkkylaskuri@veerola.fi that contains all the texts translated to some language. You can also add an issue on this project right here on gitlab. Please let me know if you want your name or nickname mentioned on the app or play store.

| Key                    | English                                  | Finnish                                    |
| ---------------------- | ---------------------------------------- | ------------------------------------------ |
| playingPlayersTitle    | Playing players                          | Pelaavat pelaajat                          |
| nonPlayingPlayersTitle | Other players                            | Muut pelaajat                              |
| noPlayersSelected      | No players selected                      | Ei pelaajia                                |
| newPlayerPlaceholder   | New player                               | Uusi pelaaja                               |
| playerSelectionTitle   | Select players                           | Valitse pelaajat                           |
| startGame              | Start game                               | Aloita peli                                |
| miss                   | Miss                                     | Huti                                       |
| gameHeader             | Game ongoing                             | Peli käynnissä                             |
| endGameButton          | End game                                 | Lopeta peli                                |
| playAgain              | Play again                               | Pelaa uudestaan                            |
| returnToStart          | Return to start                          | Lopeta peli                                |
| resultsHeader          | Results                                  | Lopputulokset                              |
| openSettings           | Settings                                 | Asetukset                                  |
| dropFromThreeMisses    | Drop from three misses                   | Kolmesta hudista tippuu                    |
| dropTo25               | Drop to 25 points when over              | Ylimenosta tippuu 25 pisteeseen            |
| settings               | Settings                                 | Asetukset                                  |
| back                   | Go back                                  | Takaisin                                   |
| useDarkTheme           | Use dark color theme                     | Käytä tummia värejä                        |
| useLargeFont           | Use large font size                      | Käytä suurempaa tekstin kokoa              |
| average                | Average                                  | Keskiarvo                                  |
| misses                 | Misses                                   | Hudit                                      |
| undo                   | Undo                                     | Peruuta                                    |
| redo                   | Redo                                     | Palauta                                    |
| playerAlreadyExists    | Player with the same name already exists | Saman niminen pelaaja on jo lisätty        |
| pinPositionHeader      | Arrange the pins into starting position  | Järjestele mölkyt aloituskuvioon           |
| showStartGameMessage   | Show help and quick settings before game | Näytä ohjeet ja pika-asetukset ennen peliä |
| gameSettings           | Game settings                            | Pelin asetukset                            |
| startMessageHeader     | Starting game                            | Pelin aloitus                              |
| selectLanguage         | Select language                          | Valitse kieli                              |
