/**
 * @format
 */

import {screen} from "@testing-library/react-native";
import * as React from "react";
import {Application} from "../App";

import {render} from "../testUtils";
import {defaultTexts} from "../react-app/Localization";

describe("Application core", () => {
    it("Should start from the add players view", () => {
        render(<Application />);
        expect(
            screen.getByText(defaultTexts["playerSelectionTitle"]),
        ).toBeTruthy();
    });
});
