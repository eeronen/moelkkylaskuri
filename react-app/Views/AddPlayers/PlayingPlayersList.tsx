import React from "react";
import {
    View,
    Text,
    Pressable,
    StyleSheet,
    TouchableOpacity,
} from "react-native";
import {useSelector} from "react-redux";
import {StoreState, useAppDispatch} from "../../store";
import {Direction, scoreActions} from "../../store/Scores";
import {UpIcon, DownIcon, DeleteIcon} from "../Common/Icons";
import {useColors, useFontSizes} from "../Styles";
import {getTestId} from "../../../testUtils";
import {useGetString} from "../../Localization/StringContext";

export const PlayingPlayerList: React.FC<{}> = () => {
    const styles = useStyles();
    const getString = useGetString();
    const playingPlayers = useSelector(
        (state: StoreState) => state.scores.present.playingPlayers,
    );

    if (playingPlayers.length === 0) {
        return (
            <View style={styles.playingPlayersContainer}>
                <Text style={styles.noPlayers}>
                    {getString("noPlayersSelected")}
                </Text>
            </View>
        );
    } else {
        return (
            <View style={styles.playingPlayersContainer}>
                {playingPlayers.map(player => (
                    <PlayingPlayer
                        key={player.playerName}
                        playerName={player.playerName}
                    />
                ))}
            </View>
        );
    }
};

const PlayingPlayer: React.FC<{playerName: string}> = ({playerName}) => {
    const dispatch = useAppDispatch();
    const styles = useStyles();

    const remove = React.useCallback(() => {
        dispatch(scoreActions.removePlayer(playerName));
    }, [dispatch, playerName]);

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={remove}>
                <Text numberOfLines={1} style={styles.playerName}>
                    {playerName}
                </Text>
            </TouchableOpacity>
            <PlayingPlayerControls playerName={playerName} />
        </View>
    );
};

const PlayingPlayerControls: React.FC<{playerName: string}> = ({
    playerName,
}) => {
    const dispatch = useAppDispatch();
    const styles = useStyles();
    const colors = useColors();
    const moveUp = React.useCallback(() => {
        dispatch(
            scoreActions.movePlayer({playerName, direction: Direction.Up}),
        );
    }, [dispatch, playerName]);

    const moveDown = React.useCallback(() => {
        dispatch(
            scoreActions.movePlayer({playerName, direction: Direction.Down}),
        );
    }, [dispatch, playerName]);

    const remove = React.useCallback(() => {
        dispatch(scoreActions.removePlayer(playerName));
    }, [dispatch, playerName]);

    return (
        <View style={styles.controls}>
            <Pressable
                onPress={moveUp}
                testID={getTestId(playerName, "moveUp")}>
                <UpIcon color={colors.darkGray} />
            </Pressable>
            <Pressable
                onPress={moveDown}
                testID={getTestId(playerName, "moveDown")}>
                <DownIcon color={colors.darkGray} />
            </Pressable>
            <Pressable
                onPress={remove}
                testID={getTestId(playerName, "remove")}>
                <DeleteIcon color={colors.darkGray} />
            </Pressable>
        </View>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        container: {
            flexDirection: "row",
            justifyContent: "space-between",
            padding: 10,
        },
        playerName: {
            fontSize: fontSize.default,
            color: colors.black,
            flexShrink: 1,
        },
        noPlayers: {
            fontSize: fontSize.default,
            color: colors.darkGray,
            marginLeft: 10,
        },
        controls: {
            flexDirection: "row",
            gap: 15,
            alignItems: "center",
            flexShrink: 0,
        },
        playingPlayersContainer: {
            marginBottom: 10,
        },
    });
};
