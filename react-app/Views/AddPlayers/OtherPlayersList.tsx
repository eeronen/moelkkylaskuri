import React from "react";
import {
    Pressable,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";
import {useSelector} from "react-redux";
import {StoreState, useAppDispatch} from "../../store";
import {applicationActions} from "../../store/Application";
import {playersActions} from "../../store/Players";
import {scoreActions} from "../../store/Scores";
import {AddIcon, DeleteIcon, EditIcon} from "../Common/Icons";
import {useColors, useFontSizes} from "../Styles";
import {NewPlayerInput} from "./NewPlayerInput";
import {getTestId} from "../../../testUtils";
import {useGetString} from "../../Localization/StringContext";

export const NonPlayingPlayerList: React.FC<{}> = () => {
    const styles = useStyles();
    const playingPlayers = useSelector(
        (state: StoreState) => state.scores.present.playingPlayers,
    );
    const allPlayers = useSelector((state: StoreState) => state.players);
    const nonPlayingPlayers = React.useMemo(
        () =>
            allPlayers.filter(
                player =>
                    playingPlayers.findIndex(
                        playingPlayer =>
                            playingPlayer.playerName === player.name,
                    ) === -1,
            ),
        [allPlayers, playingPlayers],
    );

    const editablePlayerName = useSelector(
        (state: StoreState) => state.application.editedPlayerName,
    );

    return (
        <View style={styles.allPlayersContainer}>
            {nonPlayingPlayers.map(player => (
                <View key={player.name} style={styles.container}>
                    {editablePlayerName === player.name ? (
                        <EditPlayerInput playerName={player.name} />
                    ) : (
                        <PlayerRow playerName={player.name} />
                    )}
                </View>
            ))}
            <NewPlayerInput />
        </View>
    );
};

const PlayerRow: React.FC<{playerName: string}> = ({playerName}) => {
    const dispatch = useAppDispatch();
    const styles = useStyles();

    const setAsPlaying = React.useCallback(() => {
        dispatch(scoreActions.addPlayerToGame(playerName));
    }, [dispatch, playerName]);

    return (
        <>
            <TouchableOpacity onPress={setAsPlaying}>
                <Text numberOfLines={1} style={styles.playerName}>
                    {playerName}
                </Text>
            </TouchableOpacity>
            <NonPlayingPlayerControls playerName={playerName} />
        </>
    );
};

const EditPlayerInput: React.FC<{playerName: string}> = ({playerName}) => {
    const dispatch = useAppDispatch();
    const getString = useGetString();
    const styles = useStyles();
    const existingPlayers = useSelector((state: StoreState) =>
        state.players.map(player => player.name),
    );
    const [validation, setValidation] = React.useState<null | string>(null);
    const [draftValue, setDraftValue] = React.useState(playerName);
    const onChangeText = React.useCallback(
        (newValue: string) => {
            if (existingPlayers.includes(newValue) && newValue !== playerName) {
                setValidation(getString("playerAlreadyExists"));
            } else {
                setValidation(null);
            }
            setDraftValue(newValue);
        },
        [setDraftValue, existingPlayers],
    );
    const onEndEditing = React.useCallback(() => {
        if (validation !== null || playerName === draftValue) {
            dispatch(applicationActions.stopPlayerNameEditing());
            return;
        }
        dispatch(
            playersActions.renamePlayer({
                oldName: playerName,
                newName: draftValue,
            }),
        );
    }, [dispatch, draftValue]);
    return (
        <View style={styles.editPlayerContainer}>
            <Text style={styles.validationText}>{validation}</Text>
            <TextInput
                testID={"playerNameEditInput"}
                style={StyleSheet.compose(
                    styles.playerNameInput,
                    validation !== null ? styles.invalidPlayerNameInput : {},
                )}
                value={draftValue}
                onChangeText={onChangeText}
                selectTextOnFocus={true}
                autoFocus={true}
                maxLength={20}
                onEndEditing={onEndEditing}
            />
        </View>
    );
};

const NonPlayingPlayerControls: React.FC<{playerName: string}> = ({
    playerName,
}) => {
    const dispatch = useAppDispatch();
    const styles = useStyles();
    const colors = useColors();

    const remove = React.useCallback(() => {
        dispatch(playersActions.removePlayer(playerName));
    }, [dispatch, playerName]);

    const setAsPlaying = React.useCallback(() => {
        dispatch(scoreActions.addPlayerToGame(playerName));
    }, [dispatch, playerName]);

    const edit = React.useCallback(() => {
        dispatch(applicationActions.editPlayerName(playerName));
    }, [playerName, dispatch]);

    return (
        <View style={styles.controls}>
            <Pressable
                onPress={setAsPlaying}
                testID={getTestId(playerName, "setPlaying")}>
                <AddIcon color={colors.darkGray} />
            </Pressable>
            <Pressable onPress={edit} testID={getTestId(playerName, "edit")}>
                <EditIcon color={colors.darkGray} />
            </Pressable>
            <Pressable
                onPress={remove}
                testID={getTestId(playerName, "delete")}>
                <DeleteIcon color={colors.darkGray} />
            </Pressable>
        </View>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        container: {
            flexDirection: "row",
            justifyContent: "space-between",
            padding: 10,
        },
        playerNameInput: {
            flex: 1,
            borderBottomWidth: 1,
            borderBottomColor: colors.lightGray,
            fontSize: fontSize.default,
            color: colors.black,
        },
        validationText: {
            color: colors.red,
            fontSize: fontSize.small,
            marginLeft: 10,
        },
        invalidPlayerNameInput: {
            borderColor: colors.red,
            borderBottomColor: colors.red,
            borderWidth: 1,
        },
        editPlayerContainer: {
            flexGrow: 1,
        },
        playerName: {
            fontSize: fontSize.default,
            color: colors.black,
            flexShrink: 1,
        },
        controls: {
            flexDirection: "row",
            gap: 15,
            alignItems: "center",
            flexShrink: 0,
        },
        allPlayersContainer: {
            flexGrow: 1,
        },
    });
};
