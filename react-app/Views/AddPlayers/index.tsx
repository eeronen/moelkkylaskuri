import React from "react";
import {StyleSheet, View} from "react-native";
import {useSelector} from "react-redux";
import {StoreState, useAppDispatch} from "../../store";
import {applicationActions} from "../../store/Application";
import {Button} from "../Common/Buttons";
import {Header} from "../Common/Header";
import {PlayerList} from "./PlayerList";
import {useGetString} from "../../Localization/StringContext";

export const AddPlayersView: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const canStartGame = useSelector(
        (state: StoreState) => state.scores.present.playingPlayers.length > 1,
    );

    const getString = useGetString();

    const startGame = React.useCallback(() => {
        if (canStartGame) {
            dispatch(applicationActions.startGame());
        }
    }, [dispatch, canStartGame]);
    const openSettings = React.useCallback(
        () => dispatch(applicationActions.openSettings()),
        [dispatch],
    );
    return (
        <View style={styles.viewContainer}>
            <Header
                title={getString("playerSelectionTitle")}
                buttons={[
                    {
                        text: getString("openSettings"),
                        onPress: openSettings,
                    },
                ]}
            />
            <PlayerList />
            <View style={styles.footer}>
                <Button onPress={startGame} disabled={!canStartGame}>
                    {getString("startGame")}
                </Button>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    viewContainer: {
        height: "100%",
    },
    footer: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
    },
});
