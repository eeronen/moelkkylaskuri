import {fireEvent, screen} from "@testing-library/react-native";
import * as React from "react";

import {defaultTexts} from "../../../Localization";
import {
    TEST_PLAYER_NAME,
    getTestId,
    getTestStore,
    render,
} from "../../../../testUtils";
import {AddPlayersView} from "..";
import {scoreActions} from "../../../store/Scores";

describe("Add players view", () => {
    it("Should show indication that there is no players", () => {
        render(<AddPlayersView />);
        expect(
            screen.getByText(defaultTexts["noPlayersSelected"]),
        ).toBeTruthy();
    });

    it("Should have input for new players", () => {
        render(<AddPlayersView />);
        expect(
            screen.getByPlaceholderText(defaultTexts["newPlayerPlaceholder"]),
        ).toBeTruthy();
    });

    it("Should add new players", () => {
        render(<AddPlayersView />);
        expect(
            screen.getByText(defaultTexts["noPlayersSelected"]),
        ).toBeTruthy();
        fireEvent.changeText(
            screen.getByPlaceholderText(defaultTexts["newPlayerPlaceholder"]),
            "Player1",
        );
        fireEvent(
            screen.getByPlaceholderText(defaultTexts["newPlayerPlaceholder"]),
            "submitEditing",
        );
        expect(screen.getByText("Player1")).toBeTruthy();
        expect(
            screen.queryByText(defaultTexts["noPlayersSelected"]),
        ).toBeFalsy();
    });

    it("Should remove players", () => {
        const store = getTestStore();
        render(<AddPlayersView />, store);
        expect(screen.queryByText(TEST_PLAYER_NAME + 0)).toBeTruthy();

        fireEvent(
            screen.getByTestId(getTestId(TEST_PLAYER_NAME + 0, "remove")),
            "press",
        );
        expect(
            screen.queryByTestId(getTestId(TEST_PLAYER_NAME + 0, "remove")),
        ).toBeFalsy();

        fireEvent(
            screen.getByTestId(getTestId(TEST_PLAYER_NAME + 1, "remove")),
            "press",
        );

        expect(
            screen.queryByText(defaultTexts["noPlayersSelected"]),
        ).toBeTruthy();
    });

    it("Should add players", () => {
        const store = getTestStore();
        store.dispatch(scoreActions.removePlayer(TEST_PLAYER_NAME + 0));
        store.dispatch(scoreActions.removePlayer(TEST_PLAYER_NAME + 1));

        render(<AddPlayersView />, store);
        expect(
            screen.queryByText(defaultTexts["noPlayersSelected"]),
        ).toBeTruthy();

        fireEvent(
            screen.getByTestId(getTestId(TEST_PLAYER_NAME + 0, "setPlaying")),
            "press",
        );
        expect(
            screen.queryByTestId(getTestId(TEST_PLAYER_NAME + 0, "remove")),
        ).toBeTruthy();
        expect(
            screen.queryByText(defaultTexts["noPlayersSelected"]),
        ).toBeFalsy();
    });

    it("Should rename players", async () => {
        const store = getTestStore();

        render(<AddPlayersView />, store);
        fireEvent(
            screen.getByTestId(getTestId(TEST_PLAYER_NAME + 0, "remove")),
            "press",
        );
        fireEvent(
            screen.getByTestId(getTestId(TEST_PLAYER_NAME + 0, "edit")),
            "press",
        );
        fireEvent.changeText(
            screen.getByTestId("playerNameEditInput"),
            "newName",
        );
        fireEvent(screen.getByTestId("playerNameEditInput"), "endEditing");

        expect(screen.queryByText(TEST_PLAYER_NAME + 0)).toBeFalsy();
        expect(screen.getByText("newName")).toBeTruthy();
    });
});
