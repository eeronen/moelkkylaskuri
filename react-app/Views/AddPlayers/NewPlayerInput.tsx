import React from "react";
import {StyleSheet, TextInput, View, Text} from "react-native";
import {useSelector} from "react-redux";
import {StoreState, useAppDispatch} from "../../store";
import {playersActions} from "../../store/Players";
import {useColors, useFontSizes} from "../Styles";
import {useGetString} from "../../Localization/StringContext";

export const NewPlayerInput: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const styles = useStyles();
    const colors = useColors();
    const getString = useGetString();
    const existingPlayers = useSelector((state: StoreState) =>
        state.players.map(player => player.name),
    );
    const [draftValue, setDraftValue] = React.useState("");
    const [validation, setValidation] = React.useState<null | string>(null);
    const onChangeText = React.useCallback(
        (newValue: string) => {
            if (existingPlayers.includes(newValue)) {
                setValidation(getString("playerAlreadyExists"));
            } else {
                setValidation(null);
            }
            setDraftValue(newValue);
        },
        [setDraftValue, existingPlayers],
    );
    const onSubmit = React.useCallback(() => {
        if (draftValue === "" || validation !== null) {
            return;
        }

        dispatch(playersActions.addPlayer(draftValue));
        setDraftValue("");
    }, [dispatch, draftValue]);
    return (
        <View>
            <Text style={styles.validationText}>{validation}</Text>
            <TextInput
                style={StyleSheet.compose(
                    styles.playerNameInput,
                    validation !== null ? styles.invalidPlayerNameInput : {},
                )}
                value={draftValue}
                onChangeText={onChangeText}
                placeholder={getString("newPlayerPlaceholder")}
                placeholderTextColor={colors.gray}
                onSubmitEditing={onSubmit}
                blurOnSubmit={false}
                clearButtonMode="while-editing"
            />
        </View>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        container: {
            flexDirection: "row",
            justifyContent: "space-between",
            padding: 10,
        },
        validationText: {
            color: colors.red,
            fontSize: fontSize.small,
            marginLeft: 10,
        },
        playerNameInput: {
            flex: 1,
            marginLeft: 10,
            marginRight: 10,
            borderBottomWidth: 1,
            borderBottomColor: colors.lightGray,
            fontSize: fontSize.default,
            color: colors.black,
        },
        invalidPlayerNameInput: {
            borderColor: colors.red,
            borderBottomColor: colors.red,
            borderWidth: 1,
        },
        playerName: {
            fontSize: fontSize.default,
            color: colors.black,
        },
        noPlayers: {
            fontSize: fontSize.default,
            color: colors.darkGray,
            marginLeft: 10,
        },
        controls: {flexDirection: "row", gap: 15, alignItems: "center"},
        playingPlayersContainer: {
            marginBottom: 10,
        },
        allPlayersContainer: {
            flexGrow: 1,
        },
    });
};
