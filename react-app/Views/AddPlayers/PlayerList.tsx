import React from "react";
import {ScrollView, StyleSheet, Text} from "react-native";
import {useColors, useFontSizes} from "../Styles";
import {NonPlayingPlayerList} from "./OtherPlayersList";
import {PlayingPlayerList} from "./PlayingPlayersList";
import {useGetString} from "../../Localization/StringContext";

export const PlayerList: React.FC<{}> = () => {
    const styles = useStyles();
    const getString = useGetString();
    return (
        <ScrollView style={styles.container}>
            <Text style={styles.header}>
                {getString("playingPlayersTitle")}
            </Text>
            <PlayingPlayerList />
            <Text style={styles.header}>
                {getString("nonPlayingPlayersTitle")}
            </Text>
            <NonPlayingPlayerList />
        </ScrollView>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        header: {
            margin: 5,
            fontSize: fontSize.large,
            color: colors.black,
        },
        container: {
            flex: 1,
            marginLeft: 10,
            marginRight: 10,
        },
    });
};
