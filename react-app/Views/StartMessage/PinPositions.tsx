import React from "react";
import {StyleSheet, View, Text as TextComponent} from "react-native";
import {useColors, useFontSizes} from "../Styles";
import {Circle, Svg, Text} from "react-native-svg";
import {useGetString} from "../../Localization/StringContext";

export const PinPositions: React.FC<{}> = () => {
    const colors = useColors();
    const styles = useStyles();
    const getString = useGetString();
    return (
        <View style={styles.container}>
            <TextComponent style={styles.headerText}>
                {getString("pinPositionHeader")}
            </TextComponent>
            <View style={styles.pins}>
                <Svg height={"100%"} viewBox="0 0 200 180">
                    <Circle
                        cx="50"
                        cy="25"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="50"
                        y="25"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        7
                    </Text>
                    <Circle
                        cx="100"
                        cy="25"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="100"
                        y="25"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        9
                    </Text>
                    <Circle
                        cx="150"
                        cy="25"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="150"
                        y="25"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        8
                    </Text>

                    <Circle
                        cx="25"
                        cy="68.3"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="25"
                        y="68.3"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        5
                    </Text>
                    <Circle
                        cx="75"
                        cy="68.3"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="75"
                        y="68.3"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        11
                    </Text>
                    <Circle
                        cx="125"
                        cy="68.3"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="125"
                        y="68.3"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        12
                    </Text>
                    <Circle
                        cx="175"
                        cy="68.3"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="175"
                        y="68.3"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        6
                    </Text>

                    <Circle
                        cx="50"
                        cy="111.6"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="50"
                        y="111.6"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        3
                    </Text>
                    <Circle
                        cx="100"
                        cy="111.6"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="100"
                        y="111.6"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        10
                    </Text>
                    <Circle
                        cx="150"
                        cy="111.6"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="150"
                        y="111.6"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        4
                    </Text>

                    <Circle
                        cx="75"
                        cy="154.9"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="75"
                        y="154.9"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        1
                    </Text>
                    <Circle
                        cx="125"
                        cy="154.9"
                        r="24"
                        fill="transparent"
                        stroke={colors.black}
                        strokeWidth="2"
                    />
                    <Text
                        x="125"
                        y="154.9"
                        alignmentBaseline="middle"
                        fontFamily="Arial"
                        fontSize="30"
                        textAnchor="middle"
                        stroke={colors.darkGray}
                        fill={colors.darkGray}>
                        2
                    </Text>
                </Svg>
            </View>
        </View>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        container: {
            display: "flex",
            gap: 20,
            overflow: "scroll",
            flexBasis: "50%",
        },
        headerText: {
            color: colors.black,
            fontSize: fontSize.default,
        },
        pins: {
            flexGrow: 1,
        },
    });
};
