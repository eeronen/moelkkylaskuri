import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {useColors, useFontSizes} from "../Styles";
import {SettingToggle} from "../Common/SettingsToggle";
import {settingsUtils, useSettings} from "../SettingsUtils";
import {useGetString} from "../../Localization/StringContext";

export const GameSettings: React.FC<{}> = () => {
    const styles = useStyles();
    const getString = useGetString();

    const {dropFromThreeMisses, showStartGameMessage} = useSettings();

    const toggleDropFromThreeMisses = React.useCallback(() => {
        settingsUtils.dropFromThreeMisses = !dropFromThreeMisses;
    }, [dropFromThreeMisses]);

    const toggleShowStartGameMessage = React.useCallback(() => {
        settingsUtils.showStartGameMessage = !showStartGameMessage;
    }, [showStartGameMessage]);

    return (
        <View style={styles.container}>
            <Text style={styles.header}>{getString("gameSettings")}</Text>
            <SettingToggle
                name={getString("dropFromThreeMisses")}
                onToggle={toggleDropFromThreeMisses}
                value={dropFromThreeMisses}
            />
            <SettingToggle
                name={getString("showStartGameMessage")}
                onToggle={toggleShowStartGameMessage}
                value={showStartGameMessage}
            />
        </View>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        container: {
            gap: 10,
        },
        header: {
            color: colors.black,
            fontSize: fontSize.extraLarge,
            textDecorationColor: colors.darkGray,
            textDecorationLine: "underline",
        },
    });
};
