import React from "react";
import {StyleSheet, View} from "react-native";
import {useAppDispatch} from "../../store";
import {applicationActions} from "../../store/Application";
import {PinPositions} from "./PinPositions";
import {Button} from "../Common/Buttons";
import {GameSettings} from "./GameSettings";
import {useGetString} from "../../Localization/StringContext";

export const StartMessage: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const getString = useGetString();
    const startGame = React.useCallback(() => {
        dispatch(applicationActions.startGame());
    }, []);

    const goBack = React.useCallback(() => {
        dispatch(applicationActions.returnToStart());
    }, []);

    const styles = useStyles();

    return (
        <View style={styles.body}>
            <View style={styles.content}>
                <PinPositions />
                <GameSettings />
            </View>
            <View style={styles.buttons}>
                <Button onPress={goBack} isSecondary={true}>
                    {getString("back")}
                </Button>
                <Button onPress={startGame}>{getString("startGame")}</Button>
            </View>
        </View>
    );
};

const useStyles = () => {
    return StyleSheet.create({
        body: {
            padding: 10,
            height: "100%",
            justifyContent: "space-between",
        },
        content: {
            gap: 30,
            display: "flex",
            overflow: "hidden",
        },
        buttons: {
            padding: 5,
            flexDirection: "row",
            justifyContent: "space-evenly",
        },
    });
};
