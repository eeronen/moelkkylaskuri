import React from "react";
import {
    Linking,
    Modal,
    Pressable,
    ScrollView,
    StyleSheet,
    Text,
    View,
} from "react-native";
import {settingsUtils} from "../SettingsUtils";
import {useColors, useFontSizes} from "../Styles";
import {useGetString} from "../../Localization/StringContext";
import {LanguageIcon} from "../Common/Icons";

export const LanguageSelector: React.FC<{}> = () => {
    const styles = useStyles();
    const getString = useGetString();

    const [isSelectorOpen, setIsSelectorOpen] = React.useState(false);

    const setLanguage = React.useCallback((language: string) => {
        settingsUtils.language = language;
        setIsSelectorOpen(false);
    }, []);

    return (
        <View style={styles.settingContainer}>
            <Modal
                transparent={true}
                visible={isSelectorOpen}
                onRequestClose={() => setIsSelectorOpen(false)}>
                <Text style={styles.languageSelectorHeader}>
                    {getString("selectLanguage")}:
                </Text>
                <View style={styles.modalContainer}>
                    <ScrollView>
                        <Pressable
                            style={styles.languageOption}
                            onPress={() => setLanguage("fi")}>
                            <Text style={styles.languageName}>Suomi</Text>
                        </Pressable>
                        <Pressable
                            style={styles.languageOption}
                            onPress={() => setLanguage("en")}>
                            <Text style={styles.languageName}>English</Text>
                        </Pressable>
                        <Text style={styles.translationHelpMessage}>
                            Is your language not on the list? Please help me to
                            translate the app.{" "}
                            <Text
                                style={styles.translationHelpLink}
                                onPress={() =>
                                    Linking.openURL(
                                        "https://gitlab.com/eeronen/moelkkylaskuri/-/blob/master/strings.md",
                                    )
                                }>
                                Instructions can be found here.
                            </Text>
                        </Text>
                    </ScrollView>
                </View>
            </Modal>
            <View style={styles.settingTextContainer}>
                <Pressable
                    style={styles.languageSelectorPressable}
                    onPress={() => setIsSelectorOpen(true)}>
                    <Text style={styles.settingName}>
                        {getString("selectLanguage")}
                    </Text>
                    <LanguageIcon />
                </Pressable>
            </View>
        </View>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        settingContainer: {
            justifyContent: "space-between",
            alignItems: "center",
            flexDirection: "row",
            gap: 0,
            width: "100%",
            overflow: "hidden",
        },
        settingTextContainer: {
            flexShrink: 1,
        },
        settingName: {
            fontSize: fontSize.default,
            color: colors.black,
            textDecorationStyle: "solid",
            textDecorationLine: "underline",
        },
        modalContainer: {
            flex: 1,
            backgroundColor: colors.white,
        },
        languageOption: {
            width: "100%",
            padding: 20,
            borderColor: colors.gray,
            borderBottomWidth: 1,
        },
        languageName: {
            color: colors.black,
            fontSize: fontSize.large,
        },
        languageSelectorHeader: {
            backgroundColor: colors.white,
            fontSize: fontSize.large,
            color: colors.black,
            fontWeight: "bold",
            padding: 10,
            borderBottomColor: colors.gray,
            borderBottomWidth: 1,
        },
        languageSelectorPressable: {
            flexDirection: "row",
            gap: 10,
        },
        translationHelpMessage: {
            color: colors.black,
            fontSize: fontSize.small,
            padding: 10,
        },
        translationHelpLink: {
            textDecorationStyle: "solid",
            textDecorationLine: "underline",
        },
    });
};
