import * as React from "react";
import {fireEvent, screen} from "@testing-library/react-native";
import {Application} from "../../../../App";

import {render} from "../../../../testUtils";
import {defaultTexts} from "../../../Localization";

describe("Settings", () => {
    it("Should be openable from the main screen", () => {
        render(<Application />);
        expect(screen.getByText(defaultTexts["settings"])).toBeTruthy();
        fireEvent(screen.getByText(defaultTexts["settings"]), "press");
        expect(
            screen.queryByText(defaultTexts["dropFromThreeMisses"]),
        ).toBeTruthy();
    });

    it("Closing should take back to main screen", () => {
        render(<Application />);
        fireEvent(screen.getByText(defaultTexts["settings"]), "press");
        expect(screen.queryByText(defaultTexts["back"])).toBeTruthy();

        fireEvent(screen.getByText(defaultTexts["back"]), "press");
        expect(
            screen.queryByText(defaultTexts["playerSelectionTitle"]),
        ).toBeTruthy();
    });
});
