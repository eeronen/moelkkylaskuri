import React from "react";
import {StyleSheet, View} from "react-native";
import {useAppDispatch} from "../../store";
import {applicationActions} from "../../store/Application";
import {Header} from "../Common/Header";
import {settingsUtils, useSettings} from "../SettingsUtils";
import {SettingToggle} from "../Common/SettingsToggle";
import {LanguageSelector} from "./LanguageSelector";
import {useGetString} from "../../Localization/StringContext";

export const SettingsView: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const getString = useGetString();
    const {
        dropFromThreeMisses,
        colorTheme,
        useLargerFontSize,
        showStartGameMessage,
    } = useSettings();
    const styles = useStyles();

    const toggleDropFromThreeMisses = React.useCallback(() => {
        settingsUtils.dropFromThreeMisses = !dropFromThreeMisses;
    }, [dropFromThreeMisses]);

    const toggleColorTheme = React.useCallback(() => {
        settingsUtils.colorTheme = colorTheme === "light" ? "dark" : "light";
    }, [colorTheme]);

    const toggleUseLargerFontSize = React.useCallback(() => {
        settingsUtils.useLargerFontSize = !useLargerFontSize;
    }, [useLargerFontSize]);

    const toggleShowStartGameMessage = React.useCallback(() => {
        settingsUtils.showStartGameMessage = !showStartGameMessage;
    }, [showStartGameMessage]);

    const goBack = React.useCallback(() => {
        dispatch(applicationActions.goBack());
    }, [dispatch]);

    return (
        <View>
            <Header
                title={getString("settings")}
                buttons={[{text: getString("back"), onPress: goBack}]}
            />
            <View style={styles.body}>
                <LanguageSelector />
                <SettingToggle
                    name={getString("dropFromThreeMisses")}
                    onToggle={toggleDropFromThreeMisses}
                    value={dropFromThreeMisses}
                />
                <SettingToggle
                    name={getString("useDarkTheme")}
                    onToggle={toggleColorTheme}
                    value={colorTheme === "dark"}
                />
                <SettingToggle
                    name={getString("useLargeFont")}
                    onToggle={toggleUseLargerFontSize}
                    value={useLargerFontSize}
                />
                <SettingToggle
                    name={getString("showStartGameMessage")}
                    onToggle={toggleShowStartGameMessage}
                    value={showStartGameMessage}
                />
            </View>
        </View>
    );
};

const useStyles = () => {
    return StyleSheet.create({
        body: {
            padding: 10,
            gap: 20,
            height: "100%",
        },
    });
};
