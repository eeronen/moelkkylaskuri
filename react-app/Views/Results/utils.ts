import {ScoreItem} from "../../store/Scores";

export const getRankedPlayers = (
    players: ScoreItem[],
    dropFromThreeMisses: boolean,
) => {
    const sortedPlayers = sortPlayers(players, dropFromThreeMisses);

    const rankedPlayers: ({
        playerName: string;
        place: number;
        score: number;
    } & ReturnType<typeof getPlayerKPIs>)[] = [];
    let place = 1;
    rankedPlayers.push({
        playerName: sortedPlayers[0].playerName,
        place: 1,
        score: sortedPlayers[0].score,
        ...getPlayerKPIs(sortedPlayers[0]),
    });

    for (let index = 1; index < sortedPlayers.length; index++) {
        const player = sortedPlayers[index];
        const previousPlayer = sortedPlayers[index - 1];

        const finishedOnSameRound =
            player.finishedOn === previousPlayer.finishedOn;
        const wasDropped = player.misses === 3;
        const previousWasDropped = previousPlayer.misses === 3;

        if (
            (wasDropped && !previousWasDropped) ||
            !finishedOnSameRound ||
            previousPlayer.score > player.score
        ) {
            place++;
        }

        rankedPlayers.push({
            playerName: player.playerName,
            place: place,
            score: player.score,
            ...getPlayerKPIs(player),
        });
    }

    return rankedPlayers;
};

const getPlayerKPIs = (player: ScoreItem) => {
    const scoredThrows = player.throws.filter(throwScore => throwScore !== 0);
    return {
        average: scoredThrows.length !== 0 ? average(scoredThrows) : 0,
        misses: player.throws.filter(throwScore => throwScore === 0).length,
    };
};

const average = (numbers: number[]) =>
    Math.round((numbers.reduce((sum, n) => sum + n, 0) / numbers.length) * 10) /
    10;

const enum Sort {
    FirstBeforeSecond = -1,
    Equal = 0,
    SecondBeforeFirst = 1,
}

const sortPlayers = (players: ScoreItem[], dropFromThreeMisses: boolean) =>
    [...players].sort((player1, player2) => {
        // dropped players last
        if (dropFromThreeMisses) {
            if (player1.misses === 3 && player2.misses < 3) {
                return Sort.SecondBeforeFirst;
            } else if (player1.misses < 3 && player2.misses === 3) {
                return Sort.FirstBeforeSecond;
            } else if (player1.misses === 3 && player2.misses === 3) {
                // both dropped, so let's compare who dropped first
                if (player1.finishedOn === player2.finishedOn) {
                    return Sort.Equal;
                } else {
                    return player1.finishedOn! < player2.finishedOn!
                        ? Sort.SecondBeforeFirst
                        : Sort.FirstBeforeSecond;
                }
            }
        }

        if (player1.score === 50) {
            if (player2.score === 50) {
                // both got to 50 points, so let's see who got there first
                if (player1.finishedOn === player2.finishedOn) {
                    return Sort.Equal;
                } else {
                    return player1.finishedOn! < player2.finishedOn!
                        ? Sort.FirstBeforeSecond
                        : Sort.SecondBeforeFirst;
                }
            } else {
                return Sort.FirstBeforeSecond;
            }
        }

        if (player1.score > player2.score) {
            return Sort.FirstBeforeSecond;
        } else if (player1.score < player2.score) {
            return Sort.SecondBeforeFirst;
        } else {
            return Sort.Equal;
        }
    });
