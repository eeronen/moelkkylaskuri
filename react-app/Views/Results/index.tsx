import React from "react";
import {ScrollView, StyleSheet, Text, View} from "react-native";
import {useSelector} from "react-redux";
import {StoreState, useAppDispatch} from "../../store";
import {applicationActions} from "../../store/Application";
import {Button} from "../Common/Buttons";
import {Header} from "../Common/Header";
import {useColors, useFontSizes} from "../Styles";
import {getRankedPlayers} from "./utils";
import {useSettings} from "../SettingsUtils";
import {useGetString} from "../../Localization/StringContext";

export const ResultsView: React.FC<{}> = () => {
    const styles = useStyles();
    const getString = useGetString();
    const {dropFromThreeMisses} = useSettings();
    const players = useSelector((state: StoreState) =>
        getRankedPlayers(
            state.scores.present.playingPlayers,
            dropFromThreeMisses,
        ),
    );

    return (
        <View style={styles.container}>
            <Header title={getString("resultsHeader")} buttons={[]} />
            <ScrollView contentContainerStyle={styles.scoreBoardContainer}>
                {players.map(player => (
                    <View style={styles.scoreContainer} key={player.playerName}>
                        <View style={styles.placeAndNameContainer}>
                            <Text style={styles.place}>{player.place}</Text>
                            <View>
                                <Text
                                    numberOfLines={1}
                                    style={styles.playerName}>
                                    {player.playerName}
                                </Text>
                                <View style={styles.KPIContainer}>
                                    <Text style={styles.KPIText}>
                                        {getString("average")}: {player.average}
                                    </Text>
                                    <Text style={styles.KPIText}>
                                        {getString("misses")}: {player.misses}
                                    </Text>
                                </View>
                            </View>
                        </View>
                        <Text style={styles.scoreNumber}>{player.score}</Text>
                    </View>
                ))}
            </ScrollView>
            <Footer />
        </View>
    );
};

const Footer: React.FC<{}> = () => {
    const styles = useStyles();
    const getString = useGetString();
    const dispatch = useAppDispatch();
    const returnToStart = React.useCallback(
        () => dispatch(applicationActions.returnToStart()),
        [dispatch],
    );
    const startGame = React.useCallback(
        () => dispatch(applicationActions.startGame()),
        [dispatch],
    );
    return (
        <View style={styles.footer}>
            <Button onPress={startGame}>{getString("playAgain")}</Button>
            <Button onPress={returnToStart}>
                {getString("returnToStart")}
            </Button>
        </View>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();

    return StyleSheet.create({
        container: {
            height: "100%",
        },
        scoreBoardContainer: {
            marginLeft: 10,
            marginRight: 10,
            flexGrow: 1,
        },
        placeAndNameContainer: {
            flexShrink: 1,
            flexDirection: "row",
            alignItems: "center",
            gap: 8,
        },
        scoreContainer: {
            flexDirection: "row",
            padding: 10,
            alignItems: "center",
            gap: 20,
            justifyContent: "space-between",
        },
        scoreNumber: {
            fontSize: fontSize.extraLarge,
            color: colors.black,
            flexShrink: 0,
        },
        KPIContainer: {
            flexDirection: "row",
            gap: 20,
        },
        KPIText: {
            color: colors.darkGray,
            fontSize: fontSize.small,
        },
        place: {
            fontSize: fontSize.extraLarge,
            color: colors.darkGray,
        },
        playerName: {
            fontSize: fontSize.large,
            color: colors.black,
            flexShrink: 1,
        },
        footer: {
            padding: 5,
            flexDirection: "row",
            justifyContent: "space-evenly",
        },
    });
};
