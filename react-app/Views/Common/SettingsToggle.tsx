import React from "react";
import {StyleSheet, Switch, Text, View} from "react-native";
import {useColors, useFontSizes} from "../Styles";

export const SettingToggle: React.FC<{
    name: string;
    explanation?: string;
    onToggle: () => void;
    value: boolean;
}> = ({name, onToggle, value, explanation}) => {
    const styles = useStyles();
    const colors = useColors();
    return (
        <View style={styles.settingContainer}>
            <View style={styles.settingTextContainer}>
                <Text style={styles.settingName}>{name}</Text>
                {explanation && (
                    <Text style={styles.settingExplanation}>{explanation}</Text>
                )}
            </View>
            <Switch
                style={styles.switch}
                onValueChange={onToggle}
                value={value}
                thumbColor={value ? colors.green : colors.gray}
                trackColor={{true: colors.lightGreen, false: colors.lightGray}}
            />
        </View>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        settingContainer: {
            justifyContent: "space-between",
            alignItems: "center",
            flexDirection: "row",
            gap: 0,
            width: "100%",
            overflow: "hidden",
        },
        settingTextContainer: {
            flexShrink: 1,
        },
        settingName: {
            fontSize: fontSize.default,
            color: colors.black,
        },
        settingExplanation: {
            fontSize: fontSize.small,
            color: colors.darkGray,
        },
        switch: {
            flexShrink: 0,
        },
    });
};
