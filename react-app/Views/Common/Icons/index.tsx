import * as React from "react";
import {Svg, Path, Circle} from "react-native-svg";
import {defaultIconColor, defaultIconSize} from "../../Styles";

export const AddIcon: React.FC<{color?: string; size?: number}> = ({
    color = defaultIconColor,
    size = defaultIconSize,
}) => {
    return (
        <Svg viewBox="0 0 100 100" width={size} height={size}>
            <Path
                strokeLinecap="round"
                fill="transparent"
                stroke={color}
                strokeWidth="10"
                d="M10,50 L90,50 M50,10 L50,90"></Path>
        </Svg>
    );
};

export const EditIcon: React.FC<{color?: string; size?: number}> = ({
    color = defaultIconColor,
    size = defaultIconSize,
}) => {
    return (
        <Svg viewBox="0 0 100 100" width={size} height={size}>
            <Path
                strokeLinecap="round"
                fill={color}
                stroke={color}
                strokeWidth="8"
                d="M5,95 l0,-15 l50,-50 l15,15 l-50,50 l-15,0"
            />
            <Path
                strokeLinecap="round"
                fill={color}
                stroke={color}
                strokeWidth="8"
                d="M65,20 l10,-10 q5,-5 10,0 l5,5 q5,5 0,10 l-10,10 l-15,-15"
            />
        </Svg>
    );
};

export const DeleteIcon: React.FC<{color?: string; size?: number}> = ({
    color = defaultIconColor,
    size = defaultIconSize,
}) => {
    return (
        <Svg viewBox="0 0 100 100" width={size} height={size}>
            <Path
                strokeLinecap="round"
                stroke={color}
                strokeWidth="8"
                d="M30,30 L70,70 M30,70 L70,30"
            />
            <Circle
                fill="transparent"
                stroke={color}
                strokeWidth="8"
                cy={50}
                cx={50}
                r={45}
            />
        </Svg>
    );
};

export const DownIcon: React.FC<{color?: string; size?: number}> = ({
    color = defaultIconColor,
    size = defaultIconSize,
}) => {
    return (
        <Svg viewBox="0 0 100 100" width={size} height={size}>
            <Path
                strokeLinecap="round"
                fill="transparent"
                stroke={color}
                strokeWidth="10"
                d="M10,50 L50,95 L90,50 M50,5 L50,95"></Path>
        </Svg>
    );
};

export const UpIcon: React.FC<{color?: string; size?: number}> = ({
    color = defaultIconColor,
    size = defaultIconSize,
}) => {
    return (
        <Svg viewBox="0 0 100 100" width={size} height={size}>
            <Path
                strokeLinecap="round"
                fill="transparent"
                stroke={color}
                strokeWidth="10"
                d="M10,50 L50,5 L90,50 M50,95 L50,5"></Path>
        </Svg>
    );
};

export const PlaceholderIcon: React.FC<{color?: string; size?: number}> = ({
    color = defaultIconColor,
    size = defaultIconSize,
}) => {
    return (
        <Svg viewBox="0 0 100 100" width={size} height={size}>
            <Path
                fill="transparent"
                stroke={color}
                strokeWidth="8"
                d="M10,10 L10,90 L90,90 L90,10 L10,10 L90,90 M90,10 L10,90"></Path>
        </Svg>
    );
};

export const LanguageIcon: React.FC<{color?: string; size?: number}> = ({
    color = defaultIconColor,
    size = defaultIconSize,
}) => {
    return (
        <Svg viewBox="0 0 100 100" width={size} height={size}>
            <Circle
                cx={50}
                cy={50}
                r={45}
                fill="transparent"
                strokeWidth={8}
                stroke={color}
            />
            <Path
                d="M5,35 L95,35  M5,65 L95,65"
                stroke={color}
                strokeWidth={8}
                fill="transparent"
            />
            <Path
                d="M50,5 Q90,50 50,95 Q10,50 50,5"
                stroke={color}
                strokeWidth={8}
                fill="transparent"
            />
        </Svg>
    );
};
