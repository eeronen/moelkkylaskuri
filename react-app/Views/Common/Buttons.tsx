import React from "react";
import {Pressable, StyleSheet, Text} from "react-native";
import {useColors, useFontSizes} from "../Styles";

export const Button: React.FC<{
    children: string;
    onPress: () => void;
    disabled?: boolean;
    isSecondary?: boolean;
}> = ({children, onPress, disabled, isSecondary}) => {
    const styles = useStyles();
    const containerStyle = StyleSheet.flatten([
        styles.container,
        disabled ? styles.disabledContainer : {},
        isSecondary ? styles.secondary : {},
    ]);

    const textStyle = StyleSheet.compose(
        styles.text,
        disabled ? styles.disabledText : {},
    );

    return (
        <Pressable style={containerStyle} onPress={onPress}>
            <Text style={textStyle}>{children}</Text>
        </Pressable>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        container: {
            padding: 20,
            borderRadius: 10,
            backgroundColor: colors.lightGreen,
            justifyContent: "center",
            alignItems: "center",
        },
        disabledContainer: {
            backgroundColor: colors.lightGray,
        },
        text: {
            fontSize: fontSize.large,
            color: colors.black,
        },
        disabledText: {
            color: colors.darkGray,
        },
        secondary: {
            backgroundColor: colors.lighterGreen,
            color: colors.black,
        },
    });
};
