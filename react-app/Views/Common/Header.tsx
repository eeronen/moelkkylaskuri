import * as React from "react";
import {View, Pressable, Text, StyleSheet} from "react-native";
import {useColors, useFontSizes} from "../Styles";

export interface HeaderButtonProps {
    onPress: () => void;
    text: string;
}

export const Header: React.FC<{
    title: string;
    buttons: HeaderButtonProps[];
}> = ({title, buttons}) => {
    const styles = useStyles();
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{title}</Text>
            <View style={styles.buttonsContainer}>
                {buttons.map(button => (
                    <Pressable
                        key={button.text}
                        style={styles.button}
                        onPress={button.onPress}>
                        <Text style={styles.buttonText}>{button.text}</Text>
                    </Pressable>
                ))}
            </View>
        </View>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        container: {
            padding: 12,
            flexDirection: "row",
            backgroundColor: colors.green,
            justifyContent: "space-between",
            alignItems: "center",
        },
        title: {
            fontSize: fontSize.extraLarge,
            color: colors.black,
        },
        button: {
            padding: 10,
            backgroundColor: colors.lightGreen,
            borderRadius: 10,
        },
        buttonsContainer: {
            gap: 10,
            flexDirection: "row",
        },
        buttonText: {
            color: colors.black,
            fontSize: fontSize.default,
        },
    });
};
