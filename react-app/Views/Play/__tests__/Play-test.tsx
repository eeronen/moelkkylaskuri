import * as React from "react";
import {fireEvent, screen, within} from "@testing-library/react-native";

import {defaultTexts} from "../../../Localization";
import {
    TEST_PLAYER_NAME,
    getTestId,
    getTestStore,
    render,
} from "../../../../testUtils";
import {PlayView} from "..";

describe("Playing view", () => {
    it("Should start with all the players in zero", () => {
        const store = getTestStore();
        render(<PlayView />, store);
        expect(
            within(
                screen.getByTestId(getTestId(TEST_PLAYER_NAME + 0, "score")),
            ).getByText("0"),
        ).toBeTruthy();
        expect(
            within(
                screen.getByTestId(getTestId(TEST_PLAYER_NAME + 1, "score")),
            ).getByText("0"),
        ).toBeTruthy();
    });

    it("Should add up the scores", () => {
        const store = getTestStore();
        render(<PlayView />, store);

        fireEvent(screen.getByText("10"), "press");
        fireEvent(screen.getByText("8"), "press");
        fireEvent(screen.getByText("5"), "press");
        fireEvent(screen.getByText("11"), "press");

        expect(
            within(
                screen.getByTestId(getTestId(TEST_PLAYER_NAME + 0, "score")),
            ).getByText("15"),
        ).toBeTruthy();
        expect(
            within(
                screen.getByTestId(getTestId(TEST_PLAYER_NAME + 1, "score")),
            ).getByText("19"),
        ).toBeTruthy();
    });

    it("Should undo and redo", () => {
        const store = getTestStore();
        render(<PlayView />, store);

        fireEvent(screen.getByText("10"), "press");
        fireEvent(screen.getByText("8"), "press");
        fireEvent(screen.getByText("5"), "press");
        fireEvent(screen.getByText(defaultTexts["undo"]), "press");

        expect(screen.getByText(defaultTexts["redo"])).toBeTruthy();

        expect(
            within(
                screen.getByTestId(getTestId(TEST_PLAYER_NAME + 0, "score")),
            ).getByText("10"),
        ).toBeTruthy();

        fireEvent(screen.getByText(defaultTexts["redo"]), "press");

        expect(
            within(
                screen.getByTestId(getTestId(TEST_PLAYER_NAME + 0, "score")),
            ).getByText("15"),
        ).toBeTruthy();

        fireEvent(screen.getByText(defaultTexts["undo"]), "press");
        fireEvent(screen.getByText("3"), "press");

        expect(
            within(
                screen.getByTestId(getTestId(TEST_PLAYER_NAME + 0, "score")),
            ).getByText("13"),
        ).toBeTruthy();
    });
});
