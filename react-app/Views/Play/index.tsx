import React from "react";
import {StyleSheet, View} from "react-native";
import {useSelector} from "react-redux";
import {StoreState, useAppDispatch} from "../../store";
import {applicationActions} from "../../store/Application";
import {Header} from "../Common/Header";
import {ScoreBoard} from "./ScoreBoard";
import {ScoreButtons} from "./ScoreButtons";
import {useGetString} from "../../Localization/StringContext";

export const PlayView: React.FC<{}> = () => {
    const dispatch = useAppDispatch();
    const getString = useGetString();
    const endGame = React.useCallback(
        () => dispatch(applicationActions.endGame()),
        [dispatch],
    );

    const playingPlayers = useSelector(
        (state: StoreState) =>
            state.scores.present.playingPlayers.filter(
                player => player.finishedOn === undefined,
            ).length,
    );

    React.useEffect(() => {
        if (playingPlayers === 0) {
            dispatch(applicationActions.endGame());
        }
    }, [playingPlayers, dispatch]);

    return (
        <View style={styles.container}>
            <Header
                title={getString("gameHeader")}
                buttons={[{text: getString("endGameButton"), onPress: endGame}]}
            />
            <ScoreBoard />
            <ScoreButtons />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        height: "100%",
    },
});
