import React from "react";
import {ScrollView, StyleSheet, Text, View} from "react-native";
import {useSelector} from "react-redux";
import {StoreState} from "../../store";
import {DeleteIcon} from "../Common/Icons";
import {useColors, useFontSizes} from "../Styles";
import {getTestId} from "../../../testUtils";

export const ScoreBoard: React.FC<{}> = () => {
    const styles = useStyles();
    const playingPlayers = useSelector(
        (state: StoreState) => state.scores.present.playingPlayers,
    );
    const currentThrowerNumber = useSelector(
        (state: StoreState) => state.scores.present.currentThrower,
    );

    const scrollRef = React.useRef<ScrollView>(null);

    React.useEffect(() => {
        if (scrollRef.current !== null) {
            scrollRef.current.scrollTo({
                y: Math.max(0, currentThrowerNumber - 2) * 50,
            });
        }
    }, [currentThrowerNumber]);

    return (
        <ScrollView ref={scrollRef} contentContainerStyle={styles.container}>
            {playingPlayers.map((player, index) => (
                <View
                    key={player.playerName}
                    style={StyleSheet.compose(
                        styles.scoreContainer,
                        currentThrowerNumber === index && styles.throwingPlayer,
                    )}>
                    <Text
                        numberOfLines={1}
                        style={StyleSheet.compose(
                            styles.playerName,
                            playingPlayers[index].finishedOn !== undefined &&
                                styles.finishedPlayerText,
                        )}>
                        {player.playerName}
                    </Text>
                    <View style={styles.indicators}>
                        <MissesIndicator playerName={player.playerName} />
                        <ScoreIndicator playerName={player.playerName} />
                    </View>
                </View>
            ))}
        </ScrollView>
    );
};

const MissesIndicator: React.FC<{playerName: string}> = ({playerName}) => {
    const styles = useStyles();
    const colors = useColors();
    const hasFinished = useSelector(
        (state: StoreState) =>
            state.scores.present.playingPlayers.find(
                player => player.playerName === playerName,
            )?.finishedOn !== undefined,
    );

    const misses = useSelector(
        (state: StoreState) =>
            state.scores.present.playingPlayers.find(
                score => score.playerName === playerName,
            )?.misses,
    );

    let MissIcons = [];
    for (let index = 0; index < Math.min(3, misses || 0); index++) {
        MissIcons.push(
            <DeleteIcon
                key={index}
                color={hasFinished ? colors.gray : colors.red}
            />,
        );
    }

    return <View style={styles.missesContainer}>{MissIcons}</View>;
};

const ScoreIndicator: React.FC<{playerName: string}> = ({playerName}) => {
    const styles = useStyles();
    const score = useSelector(
        (state: StoreState) =>
            state.scores.present.playingPlayers.find(
                score => score.playerName === playerName,
            )?.score,
    );
    const hasFinished = useSelector(
        (state: StoreState) =>
            state.scores.present.playingPlayers.find(
                player => player.playerName === playerName,
            )?.finishedOn !== undefined,
    );

    return (
        <Text
            style={StyleSheet.compose(
                styles.score,
                hasFinished && styles.finishedPlayerText,
            )}
            testID={getTestId(playerName, "score")}>
            {score}
        </Text>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        container: {flexGrow: 1},
        scoreContainer: {
            justifyContent: "space-between",
            alignItems: "center",
            flexDirection: "row",
            position: "relative",
            height: 50,
            paddingRight: 15,
            paddingLeft: 15,
        },
        indicators: {
            flexDirection: "row",
            gap: 20,
        },
        playerName: {
            fontSize: fontSize.default,
            color: colors.black,
            flexShrink: 1,
        },
        missesContainer: {
            flexDirection: "row",
            gap: 5,
            alignItems: "center",
            flexShrink: 0,
        },
        score: {
            alignItems: "center",
            textAlign: "right",
            fontSize: fontSize.extraLarge,
            color: colors.black,
            width: 30,
            flexShrink: 0,
        },
        throwingPlayer: {
            backgroundColor: colors.lightGreen,
            borderWidth: 1,
            borderColor: colors.green,
        },
        finishedPlayerText: {
            fontSize: fontSize.default,
            color: colors.gray,
        },
    });
};
