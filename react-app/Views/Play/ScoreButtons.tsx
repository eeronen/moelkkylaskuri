import React from "react";
import {Pressable, StyleSheet, Text, View} from "react-native";
import {StoreState, useAppDispatch} from "../../store";
import {scoreActions} from "../../store/Scores";
import {useColors, useFontSizes} from "../Styles";
import {useSelector} from "react-redux";
import {useGetString} from "../../Localization/StringContext";

export const ScoreButtons: React.FC<{}> = () => {
    const styles = useStyles();
    return (
        <View style={styles.container}>
            <View style={styles.rowContainer}>
                <ScoreButton number={1} />
                <ScoreButton number={2} />
                <ScoreButton number={3} />
            </View>

            <View style={styles.rowContainer}>
                <ScoreButton number={4} />
                <ScoreButton number={5} />
                <ScoreButton number={6} />
            </View>

            <View style={styles.rowContainer}>
                <ScoreButton number={7} />
                <ScoreButton number={8} />
                <ScoreButton number={9} />
            </View>

            <View style={styles.rowContainer}>
                <ScoreButton number={10} />
                <ScoreButton number={11} />
                <ScoreButton number={12} />
            </View>

            <View style={styles.rowContainer}>
                <UndoButton />
                <ScoreButton number={0} />
                <RedoButton />
            </View>
        </View>
    );
};

const ScoreButton: React.FC<{number: number}> = ({number}) => {
    const styles = useStyles();
    const getString = useGetString();
    const dispatch = useAppDispatch();
    const addThrow = React.useCallback(() => {
        dispatch(scoreActions.addThrow(number));
    }, [dispatch, number]);
    return (
        <Pressable style={styles.button} onPress={addThrow}>
            <Text style={styles.buttonText}>
                {number !== 0 ? number : getString("miss")}
            </Text>
        </Pressable>
    );
};

const UndoButton: React.FC<{}> = () => {
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const getString = useGetString();
    const canUndo = useSelector(
        (state: StoreState) => state.scores.past.length !== 0,
    );
    const undo = React.useCallback(() => {
        dispatch(scoreActions.undo());
    }, [dispatch]);
    return (
        <Pressable
            style={StyleSheet.compose(
                styles.button,
                canUndo ? {} : {opacity: 0},
            )}
            onPress={undo}>
            <Text style={styles.buttonText}>{getString("undo")}</Text>
        </Pressable>
    );
};

const RedoButton: React.FC<{}> = () => {
    const styles = useStyles();
    const dispatch = useAppDispatch();
    const getString = useGetString();
    const canRedo = useSelector(
        (state: StoreState) => state.scores.future.length !== 0,
    );
    const undo = React.useCallback(() => {
        dispatch(scoreActions.redo());
    }, [dispatch]);
    return (
        <Pressable
            style={StyleSheet.compose(
                styles.button,
                canRedo ? {} : {opacity: 0},
            )}
            onPress={undo}>
            <Text style={styles.buttonText}>{getString("redo")}</Text>
        </Pressable>
    );
};

const useStyles = () => {
    const colors = useColors();
    const fontSize = useFontSizes();
    return StyleSheet.create({
        container: {
            flexDirection: "column",
            flexGrow: 0,
            flexShrink: 0,
        },
        rowContainer: {
            flexDirection: "row",
        },
        button: {
            backgroundColor: colors.white,
            padding: 15,
            borderColor: colors.lightGray,
            borderWidth: 1,
            flexGrow: 1,
            flexShrink: 1,
            flexBasis: 0,
            justifyContent: "center",
            alignItems: "center",
        },
        buttonText: {
            color: colors.darkGreen,
            fontSize: fontSize.extraLarge,
        },
    });
};
