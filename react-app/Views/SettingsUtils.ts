import React from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {Appearance} from "react-native";
import {findBestAvailableLanguage} from "react-native-localize";
import {TEXTS} from "../Localization";

const SETTINGS_KEY = "settings";

class Settings {
    private settings = {
        dropFromThreeMisses: true,
        dropTo25: true,
        colorTheme: undefined as "light" | "dark" | undefined,
        useLargerFontSize: false,
        showStartGameMessage: true,
        language: "eng",
    };
    private changeListeners = new Set<() => void>();

    public set dropFromThreeMisses(value: boolean) {
        this.settings.dropFromThreeMisses = value;
        this.onSettingsChanged();
    }

    public set dropTo25(v: boolean) {
        this.settings.dropTo25 = v;
        this.onSettingsChanged();
    }

    public set colorTheme(v: "light" | "dark") {
        this.settings.colorTheme = v;
        this.onSettingsChanged();
    }

    public set useLargerFontSize(v: boolean) {
        this.settings.useLargerFontSize = v;
        this.onSettingsChanged();
    }

    public set showStartGameMessage(v: boolean) {
        this.settings.showStartGameMessage = v;
        this.onSettingsChanged();
    }

    public set language(v: string) {
        this.settings.language = v;
        this.onSettingsChanged();
    }

    constructor() {
        this.addChangeListener = this.addChangeListener.bind(this);
        this.removeChangeListener = this.removeChangeListener.bind(this);
        this.getSettings = this.getSettings.bind(this);

        const bestLocalization = findBestAvailableLanguage(
            Object.keys(TEXTS),
        ) || {languageTag: "default", isRTL: false};
        this.settings.language = bestLocalization.languageTag;

        this.initializeSettings();
    }

    public addChangeListener(listener: () => void) {
        this.changeListeners.add(listener);
    }

    public removeChangeListener(listener: () => void) {
        this.changeListeners.delete(listener);
    }

    public getSettings() {
        const {colorTheme, ...settings} = this.settings;
        return {
            colorTheme: colorTheme || Appearance.getColorScheme(),
            ...settings,
        };
    }

    private async initializeSettings() {
        const settings = JSON.parse(
            (await AsyncStorage.getItem(SETTINGS_KEY)) || "{}",
        );
        Object.assign(this.settings, settings);
        this.onSettingsChanged();
    }

    private async onSettingsChanged() {
        for (const listener of this.changeListeners) {
            listener();
        }
        await AsyncStorage.setItem(SETTINGS_KEY, JSON.stringify(this.settings));
    }
}

export const settingsUtils = new Settings();

export const useSettings = () => {
    const [settings, setSettings] = React.useState(settingsUtils.getSettings());
    React.useEffect(() => {
        const listener = () => {
            setSettings(settingsUtils.getSettings());
        };
        settingsUtils.addChangeListener(listener);
        return () => {
            settingsUtils.removeChangeListener(listener);
        };
    }, []);
    return settings;
};
