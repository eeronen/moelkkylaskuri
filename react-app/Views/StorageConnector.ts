import AsyncStorage from "@react-native-async-storage/async-storage";
import {store} from "../store";
import {playersActions} from "../store/Players";

type Players = string[];

export class StorageConnector {
    private storedValues = {
        players: [] as Players,
    };

    constructor() {
        this.initializeStateFromStorage();
    }

    private async initializeStateFromStorage() {
        await this.readFromStorage();
        store.dispatch(
            playersActions.initializePlayers(this.storedValues.players),
        );
        store.subscribe(this.onStateChange.bind(this));
    }

    private async onStateChange() {
        const state = store.getState();
        const players = state.players.map(player => player.name);
        let wasChanged = false;
        if (!deepEqual(players, this.storedValues.players)) {
            this.storedValues.players = players;
            wasChanged = true;
        }
        if (wasChanged) {
            this.commitToStorage();
        }
    }

    private async commitToStorage() {
        const valuesToCommit: [string, string][] = Object.entries(
            this.storedValues,
        ).map(([key, value]) => [key, JSON.stringify(value)]);
        await AsyncStorage.multiSet(valuesToCommit);
    }

    private async readFromStorage() {
        const storedValues = await AsyncStorage.multiGet(
            Object.keys(this.storedValues),
        );
        this.storedValues = Object.fromEntries(
            storedValues.map(([key, stringValue]) => [
                key,
                JSON.parse(stringValue ?? "[]"),
            ]),
        ) as typeof this.storedValues;
    }
}

const deepEqual = (value1: unknown, value2: unknown): boolean => {
    if (typeof value1 !== typeof value2) {
        return false;
    } else if (value1 === null || value2 === null) {
        return value2 === null;
    } else if (Array.isArray(value1) && Array.isArray(value2)) {
        if (value1.length !== value2.length) {
            return false;
        } else {
            return value1.every((value, index) =>
                deepEqual(value, value2[index]),
            );
        }
    } else if (typeof value1 === "object" && typeof value2 === "object") {
        for (const keyIn1 in value1) {
            if (!value2.hasOwnProperty(keyIn1)) {
                return false;
                // @ts-expect-error
            } else if (!deepEqual(value1[keyIn1], value2[keyIn1])) {
                return false;
            }
        }
    }
    return value1 === value2;
};
