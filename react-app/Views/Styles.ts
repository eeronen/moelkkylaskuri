import {useSettings} from "./SettingsUtils";

const HUE = 86;
const SATURATION = "75%";

export const defaultIconSize = 22;
export const defaultIconColor = "black";

export const useFontSizes = () => {
    const settings = useSettings();
    if (settings.useLargerFontSize) {
        return {
            small: 18,
            default: 20,
            large: 22,
            extraLarge: 24,
        };
    } else {
        return {
            small: 14,
            default: 18,
            large: 18,
            extraLarge: 20,
        };
    }
};

export const useColors = () => {
    const settings = useSettings();
    if (settings.colorTheme === "dark") {
        return {
            backgroundGreen: `hsl(${HUE}, ${SATURATION}, 5%)`,
            lighterGreen: `hsl(${HUE}, ${SATURATION}, 10%)`,
            lightGreen: `hsl(${HUE}, ${SATURATION}, 20%)`,
            green: `hsl(${HUE}, ${SATURATION}, 30%)`,
            darkGreen: `hsl(${HUE}, ${SATURATION}, 50%)`,
            darkerGreen: `hsl(${HUE}, ${SATURATION}, 60%)`,
            lightGray: "#555555",
            gray: "#999999",
            darkGray: "#cccccc",
            black: "#eeeeee",
            white: "#111111",
            red: "#fc423f",
        };
    } else {
        return {
            backgroundGreen: `hsl(${HUE}, ${SATURATION}, 95%)`,
            lighterGreen: `hsl(${HUE}, ${SATURATION}, 80%)`,
            lightGreen: `hsl(${HUE}, ${SATURATION}, 60%)`,
            green: `hsl(${HUE}, ${SATURATION}, 40%)`,
            darkGreen: `hsl(${HUE}, ${SATURATION}, 20%)`,
            darkerGreen: `hsl(${HUE}, ${SATURATION}, 10%)`,
            lightGray: "#cccccc",
            gray: "#999999",
            darkGray: "#555555",
            black: "#222222",
            white: "#f8f8f8",
            red: "#fc423f",
        };
    }
};
