import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {settingsUtils} from "../../Views/SettingsUtils";
import {applicationActions} from "../Application";
import {playersActions} from "../Players";

export interface ScoreItem {
    playerName: string;
    throws: number[];
    score: number;
    misses: number;
    finishedOn?: number;
}

export const enum Direction {
    Up,
    Down,
}

interface ScoresState {
    currentThrower: number;
    roundNumber: number;
    playingPlayers: ScoreItem[];
}

const scoresSlice = createSlice({
    name: "scores",
    initialState: {
        past: [] as ScoresState[],
        present: {
            currentThrower: 0,
            roundNumber: 0,
            playingPlayers: [],
        } as ScoresState,
        future: [] as ScoresState[],
    },
    reducers: {
        addPlayerToGame: (state, action: PayloadAction<string>) => {
            state.present.playingPlayers.push({
                playerName: action.payload,
                throws: [],
                misses: 0,
                score: 0,
            });
        },
        movePlayer: (
            state,
            action: PayloadAction<{playerName: string; direction: Direction}>,
        ) => {
            const {direction, playerName} = action.payload;
            const playerIndex = state.present.playingPlayers.findIndex(
                player => player.playerName === playerName,
            );
            if (
                (playerIndex === 0 && direction === Direction.Up) ||
                (playerIndex === state.present.playingPlayers.length - 1 &&
                    direction === Direction.Down)
            ) {
                return;
            }
            const playerData = state.present.playingPlayers[playerIndex];
            const swapIndex =
                direction === Direction.Up ? playerIndex - 1 : playerIndex + 1;
            state.present.playingPlayers[playerIndex] =
                state.present.playingPlayers[swapIndex];
            state.present.playingPlayers[swapIndex] = playerData;
        },
        removePlayer: (state, action: PayloadAction<string>) => {
            const removeIndex = state.present.playingPlayers.findIndex(
                scoreItem => scoreItem.playerName === action.payload,
            );
            state.present.playingPlayers.splice(removeIndex, 1);
        },
        addThrow: (state, action: PayloadAction<number>) => {
            state.past.unshift(JSON.parse(JSON.stringify(state.present)));
            state.future = [];
            if (state.past.length > 10) {
                state.past.pop();
            }

            const currentThrowingPlayer =
                state.present.playingPlayers[state.present.currentThrower];
            if (!currentThrowingPlayer) {
                throw new Error("Nobody is throwing");
            }
            currentThrowingPlayer.throws.push(action.payload);

            if (action.payload === 0) {
                currentThrowingPlayer.misses++;
            } else {
                currentThrowingPlayer.score =
                    currentThrowingPlayer.score + action.payload;
                currentThrowingPlayer.misses = 0;
            }

            if (currentThrowingPlayer.score > 50) {
                currentThrowingPlayer.score = 25;
            } else if (
                (settingsUtils.getSettings().dropFromThreeMisses &&
                    currentThrowingPlayer.misses === 3) ||
                currentThrowingPlayer.score === 50
            ) {
                currentThrowingPlayer.finishedOn = state.present.roundNumber;
            }

            if (
                state.present.playingPlayers.every(
                    player => player.finishedOn !== undefined,
                )
            ) {
                return;
            }

            do {
                state.present.currentThrower++;
                if (
                    state.present.currentThrower >=
                    state.present.playingPlayers.length
                ) {
                    state.present.currentThrower = 0;
                    state.present.roundNumber++;
                }
            } while (
                state.present.playingPlayers[state.present.currentThrower]
                    .finishedOn !== undefined
            );
        },
        undo: state => {
            if (state.past.length !== 0) {
                state.future.unshift(state.present);
                state.present = state.past.shift()!;
            }
        },

        redo: state => {
            if (state.future.length !== 0) {
                state.past.unshift(state.present);
                state.present = state.future.shift()!;
            }
        },
    },
    extraReducers: builder => {
        builder.addCase(playersActions.addPlayer, (state, action) => {
            state.present.playingPlayers.push({
                playerName: action.payload,
                throws: [],
                misses: 0,
                score: 0,
            });
        });
        builder.addCase(applicationActions.startGame, state => {
            state.past = [];
            state.future = [];
            state.present.currentThrower = 0;
            state.present.roundNumber = 0;
            state.present.playingPlayers.forEach(player => {
                player.throws = [];
                player.misses = 0;
                player.score = 0;
                player.finishedOn = undefined;
            });
        });
        builder.addCase(applicationActions.endGame, state => {
            state.present.playingPlayers.forEach(player => {
                if (player.finishedOn === undefined) {
                    player.finishedOn = state.present.roundNumber;
                }
            });
        });
    },
});

export const scoreReducer = scoresSlice.reducer;
export const scoreActions = scoresSlice.actions;
