import {createSlice, PayloadAction} from "@reduxjs/toolkit";

interface Player {
    name: string;
}

const playersSlice = createSlice({
    name: "players",
    initialState: [] as Player[],
    reducers: {
        initializePlayers: (state, action: PayloadAction<string[]>) => {
            for (const player of action.payload) {
                if (
                    state.find(playerData => playerData.name === player) ===
                    undefined
                ) {
                    state.push({name: player});
                }
            }
        },
        addPlayer: (state, action: PayloadAction<string>) => {
            if (
                state.find(playerData => playerData.name === action.payload) ===
                undefined
            ) {
                state.push({name: action.payload});
            }
        },
        removePlayer: (state, action: PayloadAction<string>) => {
            console.log();
            state.splice(
                state.findIndex(player => player.name === action.payload),
                1,
            );
        },
        renamePlayer: (
            state,
            action: PayloadAction<{oldName: string; newName: string}>,
        ) => {
            const playerIndex = state.findIndex(
                player => player.name === action.payload.oldName,
            );
            state[playerIndex].name = action.payload.newName;
        },
    },
});

export const playersActions = playersSlice.actions;
export const playersReducer = playersSlice.reducer;
