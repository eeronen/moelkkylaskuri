import {configureStore} from "@reduxjs/toolkit";
import {useDispatch, useSelector} from "react-redux";
import {applicationReducer} from "./Application";
import {playersReducer} from "./Players";
import {scoreReducer} from "./Scores";

export const makeStore = () =>
    configureStore({
        reducer: {
            application: applicationReducer,
            players: playersReducer,
            scores: scoreReducer,
        },
    });

export const store = makeStore();

export type StoreState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useAppSelector = useSelector<StoreState>;
export const useAppDispatch = useDispatch<AppDispatch>;
