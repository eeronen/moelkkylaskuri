import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {playersActions} from "../Players";
import {settingsUtils} from "../../Views/SettingsUtils";

export const enum ApplicationView {
    PlayerSelection,
    Playing,
    EndResults,
    Settings,
    StartMessage,
}

const applicationSlice = createSlice({
    name: "application",
    initialState: {
        currentView: ApplicationView.PlayerSelection,
        previousViews: [] as ApplicationView[],
        editedPlayerName: null as null | string,
    },
    reducers: {
        startGame: state => {
            state.previousViews.unshift(state.currentView);
            if (
                settingsUtils.getSettings().showStartGameMessage &&
                state.currentView !== ApplicationView.StartMessage
            ) {
                state.currentView = ApplicationView.StartMessage;
            } else {
                state.currentView = ApplicationView.Playing;
            }
        },
        openSettings: state => {
            state.previousViews.unshift(state.currentView);
            state.currentView = ApplicationView.Settings;
        },
        endGame: state => {
            state.previousViews.unshift(state.currentView);
            state.currentView = ApplicationView.EndResults;
        },
        goBack: state => {
            state.currentView =
                state.previousViews.shift() || ApplicationView.PlayerSelection;
        },
        returnToStart: state => {
            state.previousViews.unshift(state.currentView);
            state.currentView = ApplicationView.PlayerSelection;
        },
        editPlayerName: (state, action: PayloadAction<string>) => {
            state.editedPlayerName = action.payload;
        },
        stopPlayerNameEditing: state => {
            state.editedPlayerName = null;
        },
    },
    extraReducers: builder => {
        builder.addCase(playersActions.renamePlayer, state => {
            state.editedPlayerName = null;
        });
    },
});

export const applicationActions = applicationSlice.actions;
export const applicationReducer = applicationSlice.reducer;
