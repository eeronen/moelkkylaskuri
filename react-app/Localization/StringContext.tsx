import React from "react";
import {useSettings} from "../Views/SettingsUtils";
import {defaultTexts, StringKey, TEXTS} from ".";

const StringContext = React.createContext<(key: StringKey) => string>(
    () => "Internal error",
);

export const StringsProvider: React.FC<React.PropsWithChildren> = ({
    children,
}) => {
    const {language} = useSettings();

    const [strings, setStrings] = React.useState(defaultTexts);

    React.useEffect(() => {
        setStrings(TEXTS[language] || defaultTexts);
    }, [language]);

    const getString = React.useCallback(
        (key: StringKey) =>
            strings[key] || defaultTexts[key] || "<" + key + ">",
        [strings],
    );

    return (
        <StringContext.Provider value={getString}>
            {children}
        </StringContext.Provider>
    );
};

export const useGetString = () => React.useContext(StringContext);
