const finnishTexts = {
    playingPlayersTitle: "Pelaavat pelaajat",
    nonPlayingPlayersTitle: "Muut pelaajat",
    noPlayersSelected: "Ei pelaajia",
    newPlayerPlaceholder: "Uusi pelaaja",
    playerSelectionTitle: "Valitse pelaajat",
    startGame: "Aloita peli",
    miss: "Huti",
    gameHeader: "Peli käynnissä",
    endGameButton: "Lopeta peli",
    playAgain: "Pelaa uudestaan",
    returnToStart: "Lopeta peli",
    resultsHeader: "Lopputulokset",
    openSettings: "Asetukset",
    dropFromThreeMisses: "Kolmesta hudista tippuu",
    dropTo25: "Ylimenosta tippuu 25 pisteeseen",
    settings: "Asetukset",
    back: "Takaisin",
    useDarkTheme: "Käytä tummia värejä",
    useLargeFont: "Käytä suurempaa tekstin kokoa",
    average: "Keskiarvo",
    misses: "Hudit",
    undo: "Peruuta",
    redo: "Palauta",
    playerAlreadyExists: "Saman niminen pelaaja on jo lisätty",
    pinPositionHeader: "Järjestele mölkyt aloituskuvioon",
    showStartGameMessage: "Näytä ohjeet ja pika-asetukset ennen peliä",
    gameSettings: "Pelin asetukset",
    startMessageHeader: "Pelin aloitus",
    selectLanguage: "Valitse kieli",
};

const englishTexts = {
    playingPlayersTitle: "Playing players",
    nonPlayingPlayersTitle: "Other players",
    noPlayersSelected: "No players selected",
    newPlayerPlaceholder: "New player",
    playerSelectionTitle: "Select players",
    startGame: "Start game",
    miss: "Miss",
    gameHeader: "Game ongoing",
    endGameButton: "End game",
    playAgain: "Play again",
    returnToStart: "Return to start",
    resultsHeader: "Results",
    openSettings: "Settings",
    dropFromThreeMisses: "Drop from three misses",
    dropTo25: "Drop to 25 points when over",
    settings: "Settings",
    back: "Go back",
    useDarkTheme: "Use dark color theme",
    useLargeFont: "Use large font size",
    average: "Average",
    misses: "Misses",
    undo: "Undo",
    redo: "Redo",
    playerAlreadyExists: "Player with the same name already exists",
    pinPositionHeader: "Arrange the pins into starting position",
    showStartGameMessage: "Show help and quick settings before game",
    gameSettings: "Game settings",
    startMessageHeader: "Starting game",
    selectLanguage: "Select language",
};

export const defaultTexts = englishTexts;

export const TEXTS: {[localeName: string]: typeof englishTexts} = {
    default: englishTexts,
    fi: finnishTexts,
    en: englishTexts,
};

export type StringKey = keyof typeof defaultTexts;
